

CREATE DATABASE `lagou_data`;

USE `lagou_data`;

DROP TABLE IF EXISTS `position_data`;

CREATE TABLE `position_data`(
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `position_id` VARCHAR(60) NOT NULL DEFAULT '',
  `position_name` VARCHAR(60) NOT NULL DEFAULT '',
  `company_id`  VARCHAR(60) NOT NULL DEFAULT '',
  `company_short_name` VARCHAR(60) NOT NULL DEFAULT '',
  `position_first_type` VARCHAR(60) NOT NULL DEFAULT '',
  `industry_field`  VARCHAR(60) NOT NULL DEFAULT '',
  `education` VARCHAR(60) NOT NULL DEFAULT '',
  `work_year` VARCHAR(60) NOT NULL DEFAULT '',
  `city` VARCHAR(60) NOT NULL DEFAULT '',
  `position_advantage` VARCHAR(60) NOT NULL DEFAULT '',
  `create_time` VARCHAR(60) NOT NULL DEFAULT '',
  `salary` VARCHAR(60) NOT NULL DEFAULT '',
  `leader_name` VARCHAR(60) NOT NULL DEFAULT '',
  `company_name` VARCHAR(60) NOT NULL DEFAULT '',
  `company_size` VARCHAR(60) NOT NULL DEFAULT '',
  `finance_stage` VARCHAR(60) NOT NULL DEFAULT '',
  `job_nature` VARCHAR(60) NOT NULL DEFAULT '',
  `position_type` VARCHAR(60) NOT NULL DEFAULT '',
  `company_label_list` VARCHAR(60) NOT NULL DEFAULT '',
  `created_ts` DATETIME, -- 创建数据时间戳
  `updated_ts` DATETIME  -- 更新数据时间戳
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `position_data` MODIFY `create_time` DATETIME;
ALTER TABLE `position_data` ADD COLUMN `salary_min` DOUBLE DEFAULT '0' AFTER `salary`;
ALTER TABLE `position_data` ADD COLUMN `salary_max` DOUBLE DEFAULT '0' AFTER `salary_min`;


