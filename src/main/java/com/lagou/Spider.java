package com.lagou;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.slf4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class Spider implements Runnable
{
	private static Logger LOGGER = LoggerBuilder.build(Spider.class);
	
	private static final String CLAWER_URL = Constant.CLAWER_URL;
	
	private CountDownLatch latch;
	
	private String city;
	private String keyword;
	
	public Spider(){}
	
	public Spider(String city, String keyword, CountDownLatch latch)
	{
		this.city = city;
		this.keyword = keyword;
		this.latch = latch;
	}
	
	public Spider(String city, String keyword)
	{
		this.city = city;
		this.keyword = keyword;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public CountDownLatch getLatch() {
		return latch;
	}

	public void setLatch(CountDownLatch latch) {
		this.latch = latch;
	}

	private int getPageCount(String city, String keyword) throws IOException
	{
		String url = CLAWER_URL.replaceFirst("#CITY#", city);
		url = url.replaceFirst("#IS_FIRST#", "true");
		url = url.replaceFirst("#pageNo#","1");
		url = url.replaceFirst("#KEY_WORD#", keyword);
		LOGGER.debug(url);
		Response response = Jsoup.connect(url).method(Method.GET).ignoreContentType(true).execute();
		String json = response.body();
		LOGGER.debug(json);
		JSONObject jsonObj = JSON.parseObject(json);
		
		boolean isSuccess = jsonObj.getBoolean("success");
		
		if(isSuccess)
		{
			JSONObject contentObj = jsonObj.getJSONObject("content");
			
			int totalPageCount = contentObj.getIntValue("totalPageCount");
			
			return totalPageCount;
		}
		
		return 0;
	}
	
	private void clawerPage(String city,boolean isFirst,int pageNo, String keyword) throws IOException
	{
		String url = CLAWER_URL.replaceFirst("#CITY#", city);
		url = url.replaceFirst("#IS_FIRST#", isFirst?"true":"false");
		url = url.replaceFirst("#pageNo#",pageNo+"");
		url = url.replaceFirst("#KEY_WORD#", keyword);
		
		LOGGER.debug("url=>{}",url);
		
		Response response = Jsoup.connect(url).method(Method.GET).ignoreContentType(true).execute();
		String json = response.body();
		LOGGER.debug(json);
		JSONObject jsonObj = JSON.parseObject(json);
		
		boolean isSuccess = jsonObj.getBoolean("success");
		
		if(isSuccess)
		{
			JSONObject contentObj = jsonObj.getJSONObject("content");
			
			int totalCount = contentObj.getIntValue("totalCount");
			
			int totalPageCount = contentObj.getIntValue("totalPageCount");
			
			
			JSONArray resultArray = contentObj.getJSONArray("result");
			
			int pageSize = resultArray.size();

			LOGGER.info("本次检索总共{}条记录, 总共有{}页数, 当前第{}页, 本页面共有{}条数据", new Object[]{totalCount, totalPageCount, pageNo, pageSize});
			
			String filePath = "./output/data_"+city+"_"+keyword+"_"+DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd") + ".txt";
			
			File file = new File(filePath);
			
			for(int i = 0; i < pageSize; i++)
			{
				JSONObject resultObj = resultArray.getJSONObject(i);
				FileUtils.write(file, resultObj.toJSONString()+"\n", "UTF-8", true);
			}
			
		}
		
	}
	
	
	public void clawerPages(String city, String keyword) throws IOException, InterruptedException
	{
		int pageCount = getPageCount(city, keyword);
		
		for(int i = 1; i <= pageCount; i++)
		{
			if(i <= 1)
			{
				clawerPage(city, true, i, keyword);
			}
			else
			{
				clawerPage(city,false,i,keyword);
			}
			Thread.sleep(1000);
		}
		
	}
	
	@Override
	public void run() 
	{
		try 
		{
			this.clawerPages(this.city, this.keyword);
			this.latch.countDown();
		} 
		catch (Exception e) {
			LOGGER.error("error",e);
		}
	}
	
}
