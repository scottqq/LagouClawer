package com.lagou;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;

public class ETLData 
{
	
	private final static Logger LOGGER = LoggerBuilder.build(ETLData.class);
	
	private static Connection getConnection()
	{
		try
		{
			PropertiesConfiguration conf = new PropertiesConfiguration("./conf/dbconfig.properties");
			String url = conf.getString("jdbc.driver-url");
			String driverClass = conf.getString("jdbc.driver-class");
			String user = conf.getString("jdbc.user");
			String password = conf.getString("jdbc.password");
			Class.forName(driverClass);
			return DriverManager.getConnection(url, user, password);
		} 
		catch(Exception e)
		{
			LOGGER.error("error",e);
		}
		return null;
	}
	
	private static void close(Connection con)
	{
		try
		{
			if(con != null)
			{
				con.close();
			}
		} catch(Exception e) {}
	}
	
	
	public static void EtlDatas()
	{
		Connection con = null;
		Statement stmt = null;
		
		try
		{
			File dir = new File("./output");
			
			String suffix = DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd") + ".txt";
			
			FilenameFilter filter = new SuffixFileFilter(suffix);
			
			File[] files = dir.listFiles(filter);
			
			
			con = getConnection();
			stmt = con.createStatement();
			
			String deleteSQL = "DELETE FROM `position_data` WHERE DATE(`created_ts`) = '"+DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd")+"'";
			
			stmt.executeUpdate(deleteSQL);
			
			List<Position> positionList = new ArrayList<Position>();
			Set<String> positionIdSet = new HashSet<String>();
			for(File file : files)
			{
				List<String> lines = FileUtils.readLines(file, "utf-8");
				for(String line : lines)
				{
					Position position = Position.create(line);
					if(positionIdSet.add(position.getPositionId())) {
						positionList.add(position);
					}
				}
			}
			
			int iCount = 0;
			int len = positionList.size();
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < len; i++)
			{
				if(iCount == 0) 
				{
					sb.delete(0, sb.length());
					sb.append(Position.INSERT_SQL_HEADER);
				}
				
				if(iCount >= 1)
				{
					sb.append(",");
				}
				
				Position position = positionList.get(i);
				sb.append(position.toInsertSQLValue());
				iCount++;
				
				if(iCount == 2000)
				{
					stmt.executeUpdate(sb.toString());
					iCount = 0;
				}
			}
			
			if(iCount > 0 && iCount < 2000)
			{
				stmt.executeUpdate(sb.toString());
			}
		}
		catch(Exception e)
		{
			LOGGER.error("error",e);
		}
		finally
		{
			close(con);
		}
	}
	
	public static void main(String[] args) 
	{
		EtlDatas();
	}
}
