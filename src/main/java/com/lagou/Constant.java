package com.lagou;

import java.net.URLDecoder;

import org.apache.commons.configuration.PropertiesConfiguration;

public class Constant {
	
	public static String CLAWER_URL;
	
	public static String[] CITIES;
	
	public static String[] KEYWORDS;
	
	public static void getParameters() {
		
		System.setProperty("file.encoding", "UTF-8");
		
		try {
			PropertiesConfiguration props = new PropertiesConfiguration();
			props.setEncoding("UTF-8");
			props.load(URLDecoder.decode("./conf/config.ini","utf-8"));
			CLAWER_URL = props.getString("CLAWER_URL", "http://www.lagou.com/jobs/positionAjax.json?city=#CITY#&first=#IS_FIRST#&pn=#pageNo#&kd=#KEY_WORD#");
			CITIES = props.getStringArray("CITIES");
			KEYWORDS = props.getStringArray("KEYWORDS");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
