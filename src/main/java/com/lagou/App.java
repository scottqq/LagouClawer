package com.lagou;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App 
{
	
    public static void main( String[] args ) throws Exception
    {
    	Constant.getParameters();
    	String[] cities = Constant.CITIES;
    	String[] keywords = Constant.KEYWORDS;
    	
    	int threadNum = keywords.length*cities.length;
    	
    	ExecutorService es = Executors.newFixedThreadPool(threadNum);
    	
    	CountDownLatch latch = new CountDownLatch(threadNum);
    	
    	for(String city : cities)
    	{
	    	for(String keyword : keywords)
	    	{
	    		es.submit(new Spider(city,keyword,latch));
	    	}
    	}
    	
    	latch.await();
    	es.shutdown();
    	
    	ETLData.EtlDatas();
    	
    }
}
