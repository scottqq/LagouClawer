package com.lagou;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class Position 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Position.class);
	public static final String INSERT_SQL_HEADER = "INSERT INTO `position_data`(`position_id`,`position_name`,`company_id`,`company_short_name`,`position_first_type`,`industry_field`,`education`,`work_year`,`city`,`position_advantage`,`create_time`,`salary`,`salary_min`,`salary_max`,`leader_name`,`company_name`,`company_size`,`finance_stage`,`job_nature`,`position_type`,`company_label_list`,`created_ts`,`updated_ts`) VALUES ";
	
	private String positionId;
	private String positionName;
	private String companyId;
	private String companyShortName;
	private String positionFirstType;
	private String industryField;
	private String education;
	private String workYear;
	private String city;
	private String positionAdvantage;
	private String createTime;
	private String salary;
	private double salaryMin;
	private double salaryMax;
	private String leaderName;
	private String companyName;
	private String companySize;
	private String financeStage;
	private String jobNature;
	private String positionType;
	private String companyLabelList;
	
	
	public String toInsertSQL()
	{
		StringBuilder buf = new StringBuilder(INSERT_SQL_HEADER);
		buf.append(this.toInsertSQLValue());
		return buf.toString();
	}
	
	public String toInsertSQLHeader() 
	{
		return INSERT_SQL_HEADER;
	}
	
	public String toInsertSQLValue() 
	{
		StringBuilder buf = new StringBuilder();
		buf.append("('").append(this.positionId).append("','").append(this.positionName).append("','").append(this.companyId).append("','");
		buf.append(this.companyShortName).append("','").append(this.positionFirstType).append("','").append(this.industryField).append("','");
		buf.append(this.education).append("','").append(this.workYear).append("','").append(this.city).append("','").append(this.positionAdvantage).append("','");
		buf.append(this.createTime).append("','").append(this.salary).append("','").append(this.salaryMin).append("','").append(this.salaryMax).append("','").append(this.leaderName).append("','").append(this.companyName).append("','");
		buf.append(this.companySize).append("','").append(this.financeStage).append("','").append(this.jobNature).append("','").append(this.positionType).append("','");
		buf.append(this.companyLabelList).append("',now(),now())");
		return buf.toString();
	}
	
	@Override
	public String toString() 
	{
		return ToStringBuilder.reflectionToString(this).toString();
	}
	
	public static Position create(String json)
	{
		Position position = new Position();
		JSONObject obj = JSON.parseObject(json);
		position.setPositionId(obj.getString("positionId"));
		position.setPositionName(obj.getString("positionName"));
		position.setCompanyId(obj.getString("companyId"));
		position.setCompanyShortName(obj.getString("companyShortName"));
		position.setPositionFirstType(obj.getString("positionFirstType"));
		position.setIndustryField(obj.getString("industryField"));
		position.setEducation(obj.getString("education"));
		position.setWorkYear(obj.getString("workYear"));
		position.setCity(obj.getString("city"));
		
		String strPositionAdvantage = obj.getString("positionAdvantage");
		strPositionAdvantage = strPositionAdvantage.replaceAll("\'", "''");
		
		position.setPositionAdvantage(strPositionAdvantage);
		position.setCreateTime(obj.getString("createTime"));
		
		String strSalary = obj.getString("salary");
		position.setSalary(strSalary);
		
		if(StringUtils.contains(strSalary, "以上"))
		{
			String strSalaryMin = StringUtils.replaceChars(strSalary, "以上", "");
			strSalaryMin = StringUtils.replaceChars(strSalaryMin, "k", "000");
			Double salaryMin = 0.0;
			try{ salaryMin = Double.parseDouble(strSalaryMin);} catch(Exception e) { LOGGER.error("error",e);}
			position.setSalaryMin(salaryMin);
			position.setSalaryMax(Double.MAX_VALUE);
		}
		
		if(StringUtils.contains(strSalary, "以下"))
		{
			String strSalaryMax = StringUtils.replaceChars(strSalary, "以下", "");
			strSalaryMax = StringUtils.replaceChars(strSalaryMax, "k", "000");
			Double salaryMax = Double.MAX_VALUE;
			try{ salaryMax = Double.parseDouble(strSalaryMax);} catch(Exception e) { LOGGER.error("error",e);}
			position.setSalaryMin(0.0);
			position.setSalaryMax(salaryMax);
		}
		
		if(StringUtils.contains(strSalary, "-"))
		{
			String[] array = StringUtils.split(strSalary, "-");
			String strSalaryMin = StringUtils.replace(array[0], "k", "000");;
			String strSalaryMax = StringUtils.replace(array[1], "k", "000");;
			Double salaryMin = 0.0;
			Double salaryMax = Double.MAX_VALUE;
			try{ salaryMin = Double.parseDouble(strSalaryMin);} catch(Exception e) { LOGGER.error("error",e);}
			try{ salaryMax = Double.parseDouble(strSalaryMax);} catch(Exception e) { LOGGER.error("error",e);}
			position.setSalaryMin(salaryMin);
			position.setSalaryMax(salaryMax);
		}
		
		position.setLeaderName(obj.getString("leaderName"));
		position.setCompanyName(obj.getString("companyName"));
		position.setCompanySize(obj.getString("companySize"));
		position.setFinanceStage(obj.getString("financeStage"));
		position.setJobNature(obj.getString("jobNature"));
		position.setPositionType(obj.getString("positionType"));
		
		String strCompanyLabelList = obj.getString("companyLabelList");
		strCompanyLabelList = strCompanyLabelList.replaceAll("\\[", "");
		strCompanyLabelList = strCompanyLabelList.replaceAll("\\]", "");
		strCompanyLabelList = strCompanyLabelList.replaceAll("\"", "");
		strCompanyLabelList = strCompanyLabelList.replaceAll(",", "|");
		
		position.setCompanyLabelList(strCompanyLabelList);
		return position;
	}
	
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyShortName() {
		return companyShortName;
	}
	public void setCompanyShortName(String companyShortName) {
		this.companyShortName = companyShortName;
	}
	public String getPositionFirstType() {
		return positionFirstType;
	}
	public void setPositionFirstType(String positionFirstType) {
		this.positionFirstType = positionFirstType;
	}
	public String getIndustryField() {
		return industryField;
	}
	public void setIndustryField(String industryField) {
		this.industryField = industryField;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getWorkYear() {
		return workYear;
	}
	public void setWorkYear(String workYear) {
		this.workYear = workYear;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPositionAdvantage() {
		return positionAdvantage;
	}
	public void setPositionAdvantage(String positionAdvantage) {
		this.positionAdvantage = positionAdvantage;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanySize() {
		return companySize;
	}
	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}
	public String getFinanceStage() {
		return financeStage;
	}
	public void setFinanceStage(String financeStage) {
		this.financeStage = financeStage;
	}
	public String getJobNature() {
		return jobNature;
	}
	public void setJobNature(String jobNature) {
		this.jobNature = jobNature;
	}
	public String getPositionType() {
		return positionType;
	}
	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}
	public String getCompanyLabelList() {
		return companyLabelList;
	}
	public void setCompanyLabelList(String companyLabelList) {
		this.companyLabelList = companyLabelList;
	}

	public double getSalaryMin() {
		return salaryMin;
	}

	public void setSalaryMin(double salaryMin) {
		this.salaryMin = salaryMin;
	}

	public double getSalaryMax() {
		return salaryMax;
	}

	public void setSalaryMax(double salaryMax) {
		this.salaryMax = salaryMax;
	}
}
