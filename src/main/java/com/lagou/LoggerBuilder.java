package com.lagou;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

public class LoggerBuilder 
{

	static 
	{
		loadLogBackConfig();
	}
	
	public static void loadLogBackConfig() 
	{
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(lc);
		lc.reset();

		try 
		{
			configurator.doConfigure("./conf/logback.xml");
		} 
		catch (JoranException e) {}
	}
	
		
	public static Logger build(Class<?> clazz)
	{
		return LoggerFactory.getLogger(clazz);
	}
	
}
